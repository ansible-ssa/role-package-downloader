---
- name: Make sure rh_api_offline_token is defined
  ansible.builtin.assert:
    that:
      - rh_api_offline_token is defined
      - rh_api_offline_token != ""
    fail_msg: rh_api_offline_token is not defined or empty
    success_msg: rh_api_offline_token was defined

- name: Retrieve an access token
  ansible.builtin.uri:
    url: https://sso.redhat.com/auth/realms/redhat-external/protocol/openid-connect/token
    body:
      grant_type: refresh_token
      client_id: rhsm-api
      refresh_token: "{{ rh_api_offline_token }}"
    body_format: form-urlencoded
    method: POST
  register: response
  retries: 10
  delay: 5

- name: Retrieve product checksum
  ansible.builtin.set_fact:
    product_checksum: '{{ checksum.rhel8 if rh_product_name == "rhel8" else checksum.rhel9 if rh_product_name == "rhel9" else checksum.aap2 if rh_product_name == "aap2" else checksum.aap2_containerized if rh_product_name == "aap2_containerized" }}'
  when: rh_product_checksum is not defined or rh_product_checksum == ''

- name: Retrieve image download URL
  ansible.builtin.uri:
    url: https://api.access.redhat.com/management/v1/images/{{ product_checksum | default(rh_product_checksum, true) }}/download
    follow_redirects: false
    headers:
      Authorization: "Bearer {{ response.json.access_token }}"
    status_code: 307
  register: product_info
  retries: 10
  delay: 5

- name: Set result as facts
  ansible.builtin.set_fact:
    product_url: "{{ product_info.json.body.href }}"
    product_filename: "{{ product_info.json.body.filename }}"

- name: Save package path as fact
  ansible.builtin.set_fact:
    rh_package_path: "{{ rh_product_path | default('/root', true) }}/{{ rh_product_filename | default(product_filename, true) }}"

- name: Retrieve package
  ansible.builtin.get_url:
    url: "{{ product_url }}"
    dest: "{{ rh_package_path }}"
    mode: 0644
  retries: 10
  delay: 5

- name: Output
  ansible.builtin.debug:
    msg: The package {{ rh_product_filename | default(product_filename, true) }} has been downloaded in {{ rh_product_path | default('/root', true) }}
